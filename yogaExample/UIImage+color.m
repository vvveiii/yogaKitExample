//
//  UIImage+color.m
//  yogaExample
//
//  Created by vvveiii on 2018/7/12.
//  Copyright © 2018年 vvveiii. All rights reserved.
//

#import "UIImage+color.h"

@implementation UIImage (color)

+ (instancetype)imageWithColor:(UIColor *)color {
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(1, 1), NO, 0.0);
    [color setFill];
    UIRectFill(CGRectMake(0, 0, 1, 1));
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

@end
