//
//  UIColor+rgb.m
//  yogaExample
//
//  Created by vvveiii on 2018/7/12.
//  Copyright © 2018年 vvveiii. All rights reserved.
//

#import "UIColor+rgb.h"

@implementation UIColor (rgb)

+ (instancetype)colorWithRGB:(NSUInteger)rgb alpha:(CGFloat)alpha {
    return [UIColor colorWithRed:((rgb & 0xff0000) >> 16) / 255.0 green:((rgb & 0xff00) >> 8) / 255.0 blue:(rgb & 0xff) / 255.0 alpha:alpha];
}

@end
