//
//  UIImage+color.h
//  yogaExample
//
//  Created by vvveiii on 2018/7/12.
//  Copyright © 2018年 vvveiii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (color)

+ (instancetype)imageWithColor:(UIColor *)color;

@end
