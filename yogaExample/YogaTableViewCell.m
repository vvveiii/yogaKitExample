//
//  YogaTableViewCell.m
//  yogaExample
//
//  Created by vvveiii on 2018/7/9.
//  Copyright © 2018年 vvveiii. All rights reserved.
//

#import "YogaTableViewCell.h"
#import <yogaKit/UIView+Yoga.h>
#import <WebImage/SDWebImage.h>
#import "UIColor+rgb.h"
#import "UIImage+color.h"
#import "YGImageGridView.h"

@interface YogaTableViewCell () {
    YGImageGridView *_imageGridView;
}

@property(nonatomic,strong) UIView *cardView;

@end

@implementation YogaTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.selectedBackgroundView = ({
            UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
            view.backgroundColor = [UIColor colorWithRGB:0xf5f5f5 alpha:1];
            view;
        });

        [self.contentView configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.flexDirection = YGFlexDirectionColumn;
        }];

        self.cardView = [[UIView alloc] initWithFrame:CGRectZero];
        self.cardView.backgroundColor = [UIColor whiteColor];
        [self.cardView configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.flexDirection = YGFlexDirectionColumn;
            layout.flexGrow = 1;
            layout.marginBottom = YGPointValue(10);
        }];
        [self.contentView addSubview:self.cardView];

        // 头像、昵称
        UIView *headerBox = [[UIView alloc] initWithFrame:CGRectZero];
        [headerBox configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.flexDirection = YGFlexDirectionRow;
            layout.marginTop = YGPointValue(20);
            layout.marginHorizontal = YGPointValue(20);
        }];
        [self.cardView addSubview:headerBox];

        UIImageView *headimgView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [headimgView sd_setImageWithURL:@"https://tva3.sinaimg.cn/crop.0.0.800.800.50/b2664ecdjw8ew2lk0zxiwj20m80m840c.jpg"];
        [headimgView configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.width = YGPointValue(50);
            layout.height = YGPointValue(50);
        }];
        [headerBox addSubview:headimgView];

        UIView *nameBox = [[UIView alloc] initWithFrame:CGRectZero];
        [nameBox configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.flexDirection = YGFlexDirectionColumn;
            layout.flexGrow = 1;
            layout.justifyContent = YGJustifyCenter;
            layout.marginLeft = YGPointValue(10);
        }];
        [headerBox addSubview:nameBox];

        UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        nameLabel.font = [UIFont systemFontOfSize:17];
        nameLabel.text = @"CCTV5";
        [nameLabel configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.marginBottom = YGPointValue(3);
            layout.alignSelf = YGAlignFlexStart;
        }];
        [nameBox addSubview:nameLabel];

        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        timeLabel.font = [UIFont systemFontOfSize:10];
        timeLabel.text = @"2018/1/1 0:00";
        [timeLabel configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.marginTop = YGPointValue(3);
            layout.alignSelf = YGAlignFlexStart;
        }];
        [nameBox addSubview:timeLabel];

        UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [moreButton setBackgroundImage:[UIImage imageWithColor:[UIColor orangeColor]] forState:UIControlStateNormal];
        [moreButton configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.width = YGPointValue(20);
            layout.height = YGPointValue(20);
        }];
        [headerBox addSubview:moreButton];

        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        textLabel.numberOfLines = 0;
        textLabel.font = [UIFont systemFontOfSize:17];
        textLabel.text = @"【曼朱基奇加时破门 克罗地亚创历史杀进决赛】#世界杯半决赛#第二场，克罗地亚在加时赛破门，2-1击败英格兰！第5分钟，特里皮尔直接任意球直挂死角！第68分钟，弗尔萨利科传中，佩里希奇垫射追平！加时赛第109分钟，佩里希奇头球摆渡，曼朱基奇低射打入制胜球！克罗地亚首次杀进决赛，英格兰遗憾出局！#2018世界杯# LCCTV5的微博视频";
        [textLabel configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.marginTop = YGPointValue(10);
            layout.marginLeft = YGPointValue(20);
            layout.marginRight = YGPointValue(20);
        }];
        [self.cardView addSubview:textLabel];

        // 九宫格
        YGImageGridView *imageGridView = [[YGImageGridView alloc] initWithFrame:CGRectZero];
        [imageGridView configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.marginTop = YGPointValue(10);
            layout.marginLeft = YGPointValue(20);
            layout.width = YGPointValue(170);
            layout.height = YGPointValue(170);
        }];
        [self.cardView addSubview:imageGridView];
        _imageGridView = imageGridView;

        UIView *footerBox = [[UIView alloc] initWithFrame:CGRectZero];
        [footerBox configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.flexDirection = YGFlexDirectionColumnReverse;
            layout.flexGrow = 1;
        }];
        [self.cardView addSubview:footerBox];

        // 操作栏
        UIView *bottomBar = [[UIView alloc] initWithFrame:CGRectZero];
        [bottomBar configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.flexDirection = YGFlexDirectionRow;
            layout.height = YGPointValue(49);
            layout.width = YGPercentValue(100);
        }];
        [footerBox addSubview:bottomBar];

        UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        shareButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [shareButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [shareButton setTitle:NSLocalizedString(@"转发", nil) forState:UIControlStateNormal];
        [shareButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRGB:0xf5f5f5 alpha:1]] forState:UIControlStateHighlighted];
        [shareButton configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.flexGrow = 1;
        }];
        [bottomBar addSubview:shareButton];

        UIView *p1 = [[UIView alloc] initWithFrame:CGRectZero];
        p1.backgroundColor = [UIColor colorWithRGB:0xf5f5f5 alpha:1];
        [p1 configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.width = YGPointValue(0.5);
            layout.marginVertical = YGPointValue(10);
        }];
        [bottomBar addSubview:p1];

        UIButton *commentButton = [UIButton buttonWithType:UIButtonTypeCustom];
        commentButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [commentButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [commentButton setTitle:NSLocalizedString(@"评论", nil) forState:UIControlStateNormal];
        [commentButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRGB:0xf5f5f5 alpha:1]] forState:UIControlStateHighlighted];
        [commentButton configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.flexGrow = 1;
        }];
        [bottomBar addSubview:commentButton];

        UIView *p2 = [[UIView alloc] initWithFrame:CGRectZero];
        p2.backgroundColor = [UIColor colorWithRGB:0xf5f5f5 alpha:1];
        [p2 configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.width = YGPointValue(0.5);
            layout.marginVertical = YGPointValue(10);
        }];
        [bottomBar addSubview:p2];

        UIButton *likeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        likeButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [likeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [likeButton setTitle:NSLocalizedString(@"赞", nil) forState:UIControlStateNormal];
        [likeButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRGB:0xf5f5f5 alpha:1]] forState:UIControlStateHighlighted];
        [likeButton configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.flexGrow = 1;
        }];
        [bottomBar addSubview:likeButton];

        UIView *lineView = [[UIView alloc] initWithFrame:CGRectZero];
        lineView.backgroundColor = [UIColor colorWithRGB:0xf5f5f5 alpha:1];
        [lineView configureLayoutWithBlock:^(YGLayout * _Nonnull layout) {
            layout.isEnabled = YES;
            layout.height = YGPointValue(0.5);
            layout.marginHorizontal = YGPointValue(20);
        }];
        [footerBox addSubview:lineView];
    }

    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    [self.contentView.yoga applyLayoutPreservingOrigin:NO];
}

@end
