//
//  UIColor+rgb.h
//  yogaExample
//
//  Created by vvveiii on 2018/7/12.
//  Copyright © 2018年 vvveiii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (rgb)

+ (instancetype)colorWithRGB:(NSUInteger)rgb alpha:(CGFloat)alpha;

@end
